FROM        python:3.8

# Copy the requirements just to be able to pip install, and then change directory.
COPY       ./requirements.txt /bot/requirements.txt
WORKDIR    /bot

RUN        pip install -U pip                       \
           && pip install -r requirements.txt

# Different layer so that changing code and the config file won't trigger the whole dependencies thing.
# Not sure if it's a good idea to have all this baked in the image, but if not we can just switch to a volume ig
COPY       . /bot

CMD        echo "Booting up the bot..."             \
           && python -um bracky $CONFIG_FILE
