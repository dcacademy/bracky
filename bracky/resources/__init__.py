#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Module containing all resources/assets, usually non Python source files.
"""

__all__ = ["get_resource", "get_resource_async", "BASE_RESOURCES_PATH"]

import typing

import hikari

# kinda unnecessary to type this, but eh
BASE_RESOURCES_PATH: typing.Final[str] = "bracky/resources/"


def get_resource(name: str, mode: str = "r") -> typing.TextIO:
    """Simple method that just builds the absolute path for resources, opens a file and returns it."""
    return open(BASE_RESOURCES_PATH + name, mode=mode)


def get_resource_async(name: str) -> hikari.File:
    """Simple method that just builds the absolute path for resources and returns a hikari File object."""
    return hikari.File(BASE_RESOURCES_PATH + name)
