#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import asyncio
import logging
import textwrap
import typing

import hikari
import lightbulb

from bracky.plugins.bots import manager_controller
from bracky.utils import send_success, send_failure, staff_check, BrackyPlugin

_LOGGER: typing.Final[logging.Logger] = logging.getLogger("bracky.bots.manager")

CONFIRMATION_TIMEOUT: typing.Final[int] = 15
CONFIRMATION_EMOJI: typing.Final[str] = "\N{OK HAND SIGN}"


class BotManagerPlugin(BrackyPlugin):
    """
    Plugin holding all the bot management system.
    The system consists of better and more automated bot invitation pipeline and some utility commands for bot owners.

    The automated bot invitation pipeline first starts with the `bot invite` command, which takes a user ID and a
    prefix. It will produce an OAuth link clean of premissions and with the target guild already set, and will notify
    the staff team. Upon the bot being invited, the notification will be marked as done, the owner given the
    `Bot Developers` role and the bot given the `Bots` role, alongside the prefix indicator in its nickname.
    From there, Bracky will listen for nickname changes and update its records automatically and kick bots when their
    owners leave, get kicked or banned.

    The prefix can also be changed via the `bot prefix` command, the ownership transfered with `bot transfer` (staff can
    transfer any bot to anyone) and the bot can also be kicked with the `bot kick` command.
    Additionally, some inspections can be performed with the `bot info` command, which will display a bot's prefix and
    owner, or a user's bots, depending on the type of the given user.
    """

    def __init__(self, bot: lightbulb.Bot):
        super().__init__(bot)

        if self.app_config.bot_manager is None:
            # Not entirely sure what error type is best to throw here
            raise ValueError("BotManagerPlugin depends on bot_manager config group")

        self.controller = manager_controller.BotManagerController(rest=self.bot.rest, cache=self.bot.cache)

    @lightbulb.listener(hikari.MemberCreateEvent)
    async def give_roles_and_change_nick_listener(self, event: hikari.MemberCreateEvent):
        """
        If the new member is an expected bot, it will be given the Bots role, have its registered prefix set in the nick
        and the owner given the Bots Developer role. The staff notification will also be marked as complete.
        """
        if not event.member.is_bot:
            return

        bot = await self.controller.get_bot(event.user_id)
        if bot is None:
            _LOGGER.warning("Bot %s joined but the system wasn't expecting it!", event.user_id)
            return

        await self.controller.give_roles(bot)
        try:
            await self.controller.notify_owner(bot)
        except hikari.ForbiddenError:
            _LOGGER.warning("Couldn't send DM notification to user %s!", bot.owner_id)
        await self.controller.tick_invite_message(bot)

    @lightbulb.listener(hikari.MemberDeleteEvent)
    async def kick_bots_when_owner_leaves_listener(self, event: hikari.MemberDeleteEvent):
        """
        Will kick all bots a user owns if they leave or get kicked, or will update the database records if a bot leaves.
        """
        for bot in await self.controller.get_bots_for_owner(event.user_id):
            await self.controller.kick_bot(bot.bot_id)

    @lightbulb.listener(hikari.BanCreateEvent)
    async def kick_bots_when_owner_is_banned_listener(self, event: hikari.BanCreateEvent):
        """
        Will kick all bots a user owns if they get banned, or will update the database records if a bot somehow gets
        banned instead.
        """
        for bot in await self.controller.get_bots_for_owner(event.user_id):
            await self.controller.kick_bot(bot.bot_id)

    @lightbulb.listener(hikari.MemberUpdateEvent)
    async def update_records_if_bot_nick_changes_listener(self, event: hikari.MemberUpdateEvent):
        """Updates database records if the prefix indicator in a bot's nickname is changed."""
        if not event.user.is_bot:
            return

        bot_record = await self.controller.get_bot(event.user_id)
        if bot_record is None:
            _LOGGER.warning("Bot %s is not registered in the system", event.user_id)
            return

        if event.member.nickname is None:
            _LOGGER.warning("Bot %s has no nick! Setting it...", event.user_id)
            await self.controller.update_bot_nick_prefix(
                event.user_id, bot_record.prefix, username=event.member.username
            )
            return

        nick_prefix = self.controller.get_prefix_from_nick(event.member.nickname)
        if nick_prefix != bot_record.prefix:
            await self.controller.update_bot_prefix(event.user_id, nick_prefix)

    # TODO: Add checks and cooldowns

    @lightbulb.guild_only()
    @lightbulb.cooldown(5, 1, lightbulb.UserBucket)
    @lightbulb.group(name="bot", aliases=["bots"], insensitive_commands=True)
    async def bot_group(self, ctx: lightbulb.Context, target: typing.Optional[lightbulb.member_converter] = None):
        """
        Bot management system command group.

        This group houses all the needed commands to invite bots and manage them, letting you inspect bots and owners,
        changing prefixes, kicking and even transfering the ownership to someone else.

        If no valid subcommand is passed, the group will act as the `info` subcommand.
        """
        await ctx.command.get_subcommand("info").callback(self, ctx, target)

    @lightbulb.cooldown(60, 1, lightbulb.UserBucket)
    @bot_group.command(name="invite", aliases=["iv"])
    async def bot_invite_command(self, ctx: lightbulb.Context, bot_id: int, prefix: str):
        """
        Creates a bot invite for staff.

        This command will create a clean bot invite URL and notify the staff team. Upon the bot joining, it will receive
        the `Bots` role, its nick will be set with the given prefix and you the `Bot Developers` role,
        if not present already.

        The maximum prefix length is **`10`**, to prevent abusing.
        """
        if ctx.channel_id != self.app_config.bot_manager.invite_channel:
            await send_failure(
                ctx.channel, f"Please run this command in <#{self.app_config.bot_manager.invite_channel}>."
            )
            return

        bot_candidate = await self.controller.get_bot(bot_id)
        if bot_candidate is not None:
            await send_failure(ctx.channel, "Bot is already registered")
            return

        try:
            await self.controller.create_invite(bot_id, ctx.author.id, prefix)
        except hikari.NotFoundError:
            await send_failure(ctx.channel, "Couldn't find bot with that ID")

    @lightbulb.cooldown(5, 1, lightbulb.UserBucket)
    @bot_group.command(name="info", aliases=["if"])
    async def bot_info_command(
        self, ctx: lightbulb.Context, target: typing.Optional[lightbulb.member_converter] = None
    ):
        """
        Shows information about a bot or bot owner.

        This command will show relevant information about the given target, depending on whether it's a bot or a human
        user.
        If it's a bot, it will display who the registered owner is and what its prefix is currently set to.
        If it's a human, it will display a list of all the bots owned by that user.
        """
        target = target or ctx.author
        if target.is_bot:
            bot_record = await self.controller.get_bot(target.id)
            if bot_record is None:
                # Pinging staff because the database records need to be fixed
                await send_failure(
                    ctx.channel, f"**Bot isn't registered!**\n<@&{self.app_config.constants.staff_role}>"
                )
                return

            owner = ctx.bot.cache.get_user(hikari.Snowflake(bot_record.owner_id))
            if owner is None:
                await send_failure(
                    ctx.channel, f"**Can't find this bot's owner!**\n<@&{self.app_config.constants.staff_role}>"
                )
                return

            await ctx.respond(
                textwrap.dedent(
                    f"""
                    >>> \N{INFORMATION SOURCE} **__Bot information__  [{target.mention}]**
                    **Owner:** `{owner.username}#{owner.discriminator}`
                    **Prefix:** `{bot_record.prefix}`
                    **Added at:** `{target.joined_at.strftime("%x at %X")}`
                    """
                )
            )
            return

        lines: typing.List[str] = []
        async for bot in self.controller.get_bots_for_owner_iter(target.id):
            if ctx.bot.cache.get_member(ctx.guild_id, hikari.Snowflake(bot.bot_id)) is None:
                identifier = f"**[Pending]** `{bot.bot_id}`"
            else:
                identifier = f"<@{bot.bot_id}>"

            lines.append(f"\N{BULLET} {identifier} - `{bot.prefix}`")

        if lines:
            nl = "\n"
            body = textwrap.dedent(
                f"""
                **Total:** `{len(lines)}`
                **──────────────**
                {nl.join(lines)}
                """
            )
        else:
            body = "\n`This user doesn't own any bots at the moment`"

        await ctx.respond(f">>> \N{INFORMATION SOURCE} **__Owner information__**\n{body}")

    @lightbulb.cooldown(15, 1, lightbulb.UserBucket)
    @bot_group.command(name="prefix", aliases=["pf"])
    async def bot_prefix_command(self, ctx: lightbulb.Context, bot: lightbulb.member_converter, new_prefix: str):
        """
        Changes the prefix of a bot you own.

        This command will change the records of your bot's prefix and its nick to the new prefix you pass.
        Don't forget that the prefix must be **`10`** characters at maximum.

        Members from the staff team can change your bot's prefix, for moderation purposes.
        """
        if not bot.is_bot:
            await send_failure(ctx.channel, "You've passed a human account")
            return

        bot_record = await self.controller.get_bot(bot.id)
        author = ctx.bot.cache.get_member(ctx.guild_id, ctx.author.id)
        if author.id != bot_record.owner_id and self.app_config.constants.staff_role not in author.role_ids:
            await send_failure(ctx.channel, "You do not have permission to change this bot's prefix!")
            return

        await self.controller.update_bot_prefix(bot.id, new_prefix)
        await self.controller.update_bot_nick_prefix(bot.id, new_prefix, username=bot.username)
        await send_success(
            ctx.channel, f"Successfully changed {bot.mention} prefix from `{bot_record.prefix}` to `{new_prefix}`!"
        )

    @lightbulb.cooldown(15, 1, lightbulb.UserBucket)
    @bot_group.command(name="kick")
    async def bot_kick_command(
        self, ctx: lightbulb.Context, bot: lightbulb.member_converter, *, reason: typing.Optional[str] = None
    ):
        """
        Kicks the given bot.

        This command will kick the given bot, if you own it, asking for confirmation first, via a reaction dialog.
        You can optionally pass a reason to be appended onto the audit log action.

        > \N{WARNING SIGN} Be careful because you will have to reinvite your bot if you make a mistake.
        """
        bot_record = await self.controller.get_bot(bot.id)
        if ctx.author.id != bot_record.owner_id:
            # Still allowing staff to run this command, more for consistency than anything else really,
            # since they can already kick members.
            author = ctx.bot.cache.get_member(ctx.guild_id, ctx.author.id)
            if author is None:
                await send_failure(ctx.channel, "You're not this bot's owner but I couldn't check your roles")

            if self.app_config.constants.staff_role not in author.role_ids:
                await send_failure(ctx.channel, "You do not have permission to kick this bot")
                return

            # Staff bypasses the confirmation dialog
            # Could probably refactor this, since it's also used at the end of the function
            await self.controller.kick_bot(bot.id, reason=reason)
            message = f"Successfully kicked `{bot.username}#{bot.discriminator}`!"
            if reason is not None:
                message = message[:-1] + f" with reason `{reason}`."
            await send_success(ctx.channel, message)
            return

        dialog = await ctx.respond(
            ">>> \N{WARNING SIGN} Are you sure? You will have to reinvite the bot in case you make a mistake.\n "
            f"React with {CONFIRMATION_EMOJI} to confirm this action. "
            f"This will timeout after {CONFIRMATION_TIMEOUT} seconds."
        )

        def predicate(event: hikari.GuildReactionAddEvent) -> bool:
            # Maybe it's safe to remove the channel check?
            return (
                event.guild_id == self.app_config.constants.guild
                and event.user_id == ctx.author.id
                and event.channel_id == ctx.channel_id
                and event.message_id == ctx.message_id
                and event.emoji.name == CONFIRMATION_EMOJI
            )

        try:
            # We don't care about the reaction result object, just that something matches the
            # predicate within the timeout.
            await ctx.bot.wait_for(hikari.GuildReactionAddEvent, timeout=CONFIRMATION_TIMEOUT, predicate=predicate)
        except asyncio.TimeoutError:
            await dialog.edit(dialog.content + "\n\n\N{TIMER CLOCK} ***Timed out!***")

        await self.controller.kick_bot(bot.id, reason=reason)

        message = f"Successfully kicked `{bot.username}#{bot.discriminator}`!"
        if reason is not None:
            # Removing last char, aka exclamation mark
            message = message[:-1] + f" with reason `{reason}`."
        await send_success(ctx.channel, message)

    @lightbulb.cooldown(60, 1, lightbulb.UserBucket)
    @bot_group.command(name="transfer")  # no aliaes on purpose
    async def bot_transfer_command(
        self, ctx: lightbulb.Context, bot: lightbulb.member_converter, new_owner: lightbulb.member_converter
    ):
        """
        Transfers the ownership of a bot to someone else.

        This command will change the registered owner of a bot to the given user, requiring both the current owner and
        future one to agree to the change, via a reaction dialog. Only the current owner of a bot can initiate the
        process, of course.

        > \N{WARNING SIGN} Be careful you choose the right user when transfering the ownership.

        Members from the staff team can transfer any bot from anyone to anyone, for moderation purposes.
        """
        if new_owner.is_bot:
            await send_failure(ctx.channel, "Bot owner must not be a bot...")
            return

        bot_record = await self.controller.get_bot(bot.id)
        if ctx.author.id != bot_record.owner_id:
            author = ctx.bot.cache.get_member(ctx.guild_id, ctx.author.id)
            if author is None:
                await send_failure(ctx.channel, "You're not this bot's owner but I couldn't check your roles")
                return

            if self.app_config.constants.staff_role not in author.role_ids:
                await send_failure(ctx.channel, "You do not have permission to kick this bot")
                return

            # Staff bypasses the confirmation dialog
            await self.controller.update_bot_owner(bot.id, new_owner.id)
            await send_success(ctx.channel, f"Successfully transferred {bot.mention} to `{new_owner.display_name}`!")
            return

        dialog = await ctx.respond(
            ">>> Are you sure? Make sure you have passed the right user.\n"
            f"You and {new_owner.mention} must react with {CONFIRMATION_EMOJI} to confirm this action. "
            f"This will timeout after {CONFIRMATION_TIMEOUT} seconds."
        )

        def predicate(user_id: int) -> typing.Callable[[hikari.GuildReactionAddEvent], bool]:
            def inner(event: hikari.GuildReactionAddEvent) -> bool:
                # Maybe it's safe to remove the channel check?
                return (
                    event.guild_id == self.app_config.constants.guild
                    and event.user_id == user_id
                    and event.channel_id == ctx.channel_id
                    and event.message_id == dialog.id
                    and event.emoji.name == CONFIRMATION_EMOJI
                )

            return inner

        try:
            # Wait for the two reactions, and since we don't care about the event itself,
            # just that something has matched the predicate, we don't capture the result.
            await asyncio.gather(
                ctx.bot.wait_for(
                    hikari.GuildReactionAddEvent, predicate=predicate(ctx.author.id), timeout=CONFIRMATION_TIMEOUT
                ),
                ctx.bot.wait_for(
                    hikari.GuildReactionAddEvent, predicate=predicate(new_owner.id), timeout=CONFIRMATION_TIMEOUT
                ),
            )
        except asyncio.TimeoutError:
            await dialog.edit(dialog.content + "\n\n\N{TIMER CLOCK} ***Timed out!***")
            return

        await self.controller.update_bot_owner(bot.id, new_owner.id)
        await send_success(ctx.channel, f"Successfully transferred {bot.mention} to `{new_owner.display_name}`!")

    @staff_check()
    @bot_group.command(name="register")
    async def register_bot_command(
        self, ctx: lightbulb.Context, bot: lightbulb.member_converter, owner: lightbulb.member_converter, prefix: str
    ):
        """
        Manually registers a new bot.

        Staff-only command that manually registers a bot, in case it wasn't registered due to some system failure.
        """
        await self.controller.create_bot(
            bot_id=bot.id, owner_id=owner.id, prefix=prefix, invite_message_id=ctx.message_id
        )
        await send_success(ctx.channel, f"Successfully registered {bot.mention}!")


def load(bot: lightbulb.Bot):
    bot.add_plugin(BotManagerPlugin(bot))


def unload(bot: lightbulb.Bot):
    bot.remove_plugin("BotManagerPlugin")
