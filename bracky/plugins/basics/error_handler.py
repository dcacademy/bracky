#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import textwrap
import traceback
import types
import typing

import hikari
import lightbulb

from bracky import config
from bracky.resources import error_responses
from bracky.utils import bracky_plugin

MAX_LENGTH: typing.Final[int] = 2000
PAGINATOR_PAGE_START: typing.Final[str] = ">>> "
CODE_BLOCK_START: typing.Final[str] = "```py\n"
CODE_BLOCK_END: typing.Final[str] = "```"
PAGINATOR_SPACE: typing.Final[int] = MAX_LENGTH - (len(PAGINATOR_PAGE_START) + len(CODE_BLOCK_START) + len(CODE_BLOCK_END))


def _paginate_error(
    message: str, exc_info: typing.Tuple[typing.Type[Exception], Exception, typing.Optional[types.TracebackType]]
) -> typing.Sequence[str]:
    """Paginate an error message to send in Discord messages."""
    error_string = "".join(traceback.format_exception(*exc_info)).replace("`", "\\`")

    message_length = MAX_LENGTH - (len(message) + len(CODE_BLOCK_START) + len(CODE_BLOCK_END))
    chunks = [message + CODE_BLOCK_START + error_string[:message_length] + CODE_BLOCK_END]
    error_string = error_string[message_length:]

    while error_string:
        chunks.append(PAGINATOR_PAGE_START + CODE_BLOCK_START + error_string[:PAGINATOR_SPACE] + CODE_BLOCK_END)
        error_string = error_string[PAGINATOR_SPACE:]

    return chunks


class ErrorHandlerPlugin(bracky_plugin.BrackyPlugin):
    """Small plugin that handles errors according to the error_responses resource."""

    def __init__(self, bot: lightbulb.Bot):
        self.config = config.get_config()

        super().__init__(bot)

    @lightbulb.listener(lightbulb.CommandErrorEvent)
    async def handle_command_errors(self, event: lightbulb.CommandErrorEvent) -> None:
        """Handles errors by invoking the set actions in error_responses.RESPONSES."""
        action = error_responses.RESPONSES.get(type(event.exception))
        if action is not None:
            await action(event)
            return

        await event.message.respond("> \N{COLLISION SYMBOL} An unexpected error occurred. Please retry later.")

        channel_mention = "Direct Message"
        msg_link = ""
        if event.context.guild_id is not None:
            channel_mention = f"<#{event.context.channel.id}>"
            msg_link = f"({event.message.make_link(event.context.guild_id)})"

        message = textwrap.dedent(
            f"""
            <@&{self.config.constants.staff_role}>
            >>> **Unexpected error in `{event.command.name}` command**

            **Invoker:** `{event.context.author.username}#{event.context.author.discriminator}`
            **Channel:** {channel_mention}
            **Message:** `{event.message.id}` {msg_link}

            """
        )
        chunks = _paginate_error(message, event.exc_info)
        for chunk in chunks:
            await event.app.rest.create_message(
                self.config.constants.bracky_logs_channel, chunk
            )

    @lightbulb.listener(hikari.ExceptionEvent)
    async def handle_listener_errors(self, event: hikari.ExceptionEvent):
        """Sends a ping to staff in the configured channel."""
        message = textwrap.dedent(
            f"""
            <@&{self.config.constants.staff_role}>
            >>> **Unexpected error in `{event.failed_callback.__name__}` listener for `{event.failed_event.__class__.__name__}`**

            """
        )
        chunks = _paginate_error(message, event.exc_info)
        for chunk in chunks:
            await event.app.rest.create_message(
                self.config.constants.bracky_logs_channel, chunk
            )


def load(bot: lightbulb.Bot):
    bot.add_plugin(ErrorHandlerPlugin(bot))


def unload(bot: lightbulb.Bot):
    bot.remove_plugin("ErrorHandlerPlugin")
