#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import typing

import lightbulb
import hikari

from bracky import config
from bracky.utils import patterns, BrackyPlugin


def resolve_role_id_from_string(arg: lightbulb.WrappedArg) -> typing.Optional[int]:
    ctx, role_str = arg.context, arg.data
    if role_str.isdigit():
        return int(role_str)
    elif match := patterns.ROLE_MENTION.match(role_str):
        return int(match.group("id"))
    else:
        roles = ctx.bot.cache.get_roles_view_for_guild(ctx.guild_id).values()
        if (role := lightbulb.utils.get(roles, name=role_str)) is not None:
            return role.id

    return None


class SelfAssignableRolesPlugin(BrackyPlugin):
    def __init__(self, bot: lightbulb.Bot):
        super().__init__(bot)
        self.assignable_roles: typing.List[hikari.Snowflake] = [
            hikari.Snowflake(role_id) for role_id in self.app_config.assignable_roles
        ]

    @lightbulb.bot_has_guild_permissions(hikari.Permissions.MANAGE_ROLES)
    @lightbulb.guild_only()
    @lightbulb.group(name="selfrole", aliases=["sar"])
    async def selfrole_command(self, ctx: lightbulb.Context):
        """Base command group for self assignable roles."""
        await ctx.command.get_subcommand("list").callback(self, ctx)

    @selfrole_command.command(name="get", aliases=["g", "give"])
    async def selfrole_get_command(self, ctx: lightbulb.Context, *, role_id: resolve_role_id_from_string):
        """Give yourself a self-assignable role."""
        if role_id is None or role_id not in self.assignable_roles or role_id in ctx.member.role_ids:
            await ctx.respond("No assignable role matching the search term was found.")
            return

        try:
            await ctx.bot.rest.add_role_to_member(ctx.guild_id, ctx.author.id, role_id, reason="Self-assigned")
        except hikari.ForbiddenError:
            await ctx.respond("Adding role failed.")
        else:
            await ctx.respond("Role given successfully.")

    @selfrole_command.command(name="remove", aliases=["rm", "take"])
    async def selfrole_remove_command(self, ctx: lightbulb.Context, *, role_id: resolve_role_id_from_string):
        """Remove a self-assignable role from yourself."""
        if role_id is None or role_id not in self.assignable_roles or role_id not in ctx.member.role_ids:
            await ctx.respond("You cannot remove that role from yourself.")
            return

        try:
            await ctx.bot.rest.remove_role_from_member(ctx.guild_id, ctx.author.id, role_id, reason="Self-removed")
        except hikari.ForbiddenError:
            await ctx.respond("Removing role failed.")
        else:
            await ctx.respond("Role removed successfully.")

    @selfrole_command.command(name="list", aliases=["ls"])
    async def selfrole_list_command(self, ctx: lightbulb.Context):
        """Show all roles that can be self assigned."""
        msg = [
            ">>> **Self assignable roles:**\n\n",
            *[f"<@&{role_id}> - {role_id}\n" for role_id in self.assignable_roles],
            f"\n`{ctx.clean_prefix}selfrole get <id|name|mention>`",
        ]
        await ctx.respond("".join(msg), role_mentions=False)


def load(bot: lightbulb.Bot):
    bot.add_plugin(SelfAssignableRolesPlugin(bot))


def unload(bot: lightbulb.Bot):
    bot.remove_plugin("SelfAssignableRolesPlugin")
