#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

__all__ = ["Bracky"]

import itertools
import logging
import typing

import hikari
import lightbulb

from bracky.config import get_config
from bracky import database
from bracky.resources import get_resource_async
from bracky.utils import BrackyPlugin, RoleAwareCooldownManager


_LOGGER = logging.getLogger("bracky.bot")


class Bracky(lightbulb.Bot):
    """Subclass of lightbulb's Bot to add some spark here and there that suits us."""

    def __init__(self):
        self.categories: typing.Dict[str, typing.List[lightbulb.Plugin]] = {}
        self.command_names: typing.Iterable[str] = []
        self.config = get_config().bot

        super().__init__(
            prefix=self.config.command_prefix,
            insensitive_commands=self.config.case_insensitive,
            token=self.config.token,
            banner=None,
            intents=hikari.Intents.ALL_UNPRIVILEGED | hikari.Intents.GUILD_MEMBERS,
        )

        self._load_extensions()
        self.subscribe(hikari.StartedEvent, self._print_banner_on_ready)
        self.subscribe(hikari.StartingEvent, self._start_tortoise_on_start)

    @staticmethod
    async def _start_tortoise_on_start(_):
        """Calls the Tortoise starting logic when the bot is also starting."""
        _LOGGER.info("Starting Tortoise")
        await database.start_tortoise()

    def _load_extensions(self):
        """Loads extensions taking the configured blacklist and additional extension into consideration."""
        _LOGGER.info("Loading extensions")
        # TODO: This is just temporary, the proper logic has to be written.
        self.load_extension("bracky.plugins.basics.error_handler")
        self.load_extension("bracky.plugins.basics.help")
        self.load_extension("bracky.plugins.basics.pingpong")
        self.load_extension("bracky.plugins.bots.manager")
        self.load_extension("bracky.plugins.gatekeeper.welcomer")
        self.load_extension("bracky.plugins.utility.bump_warden")
        self.load_extension("bracky.plugins.utility.role_menus")

        self.load_extension("filament.exts.superuser")

        self.command_names = list(itertools.chain.from_iterable([c.name] + list(c.aliases) for c in self.commands))

    async def _print_banner_on_ready(self, _):
        """Reads the banner file and prints it when the bot is ready."""
        async with get_resource_async("ready_banner.txt").stream() as afp:
            raw_bytes = await afp.read()
            raw_text = raw_bytes.decode("utf8")

        # TODO: Temporarily disabling the session ID, while dav thinks if it's a good idea to expose it
        # formatted_text = raw_text.format(USER_ID=self.me.id, SESSION_ID=event.session_id)
        formatted_text = raw_text.format(USER_ID=self.me.id)

        print(formatted_text)

    def add_plugin(self, plugin: lightbulb.Plugin) -> None:
        """
        Implementation of plugin registration logic that modifies cooldown managers to be staff role-aware, in order to
        allow staff members to bypass cooldowns.
        """
        # Might do something more fancy in the future, like having a config for each external extension, but that's a
        # thing for the extension (un)loading merge.
        category: str = "external"
        if isinstance(plugin, BrackyPlugin):
            category = plugin.category

        self.categories.setdefault(category, []).append(plugin)

        for cmd in plugin.walk_commands():
            if cmd.cooldown_manager is not None:
                cmd.cooldown_manager = RoleAwareCooldownManager(
                    cmd.cooldown_manager.length,
                    cmd.cooldown_manager.usages,
                    cmd.cooldown_manager.bucket,
                    roles={get_config().constants.staff_role},
                )

        super().add_plugin(plugin)
