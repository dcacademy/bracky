#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Module containing Tortoise ORM models and miscelaneous functions for stuff like database setup.
"""

import logging

import tortoise
import yarl

from bracky.config import get_config

from bracky.database.models import *


_LOGGER = logging.getLogger("bracky.database")


async def start_tortoise(*, create_schemas: bool = True) -> None:
    """Sets up the Tortoise connection and modules, and optionally creates the needed tables (True by default)."""
    config = get_config().database

    _LOGGER.info("Initializing Tortoise and connecting to PostgreSQL")
    db_url = yarl.URL.build(
        scheme="postgres",
        user=config.user,
        password=config.password,
        host=config.host,
        port=config.port,
        path="/" + config.database,
    )
    await tortoise.Tortoise.init(db_url=str(db_url), modules={"main": ["bracky.database"]})

    if create_schemas:
        _LOGGER.info("Generating schemas with Tortoise")
        await tortoise.Tortoise.generate_schemas()
