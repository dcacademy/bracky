#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
This file houses all Tortoise models used in Bracky.
"""

__all__ = ["Boot", "MAX_PREFIX_LENGTH", "Bot"]

import tortoise


class Boot(tortoise.Model):
    """
    Simple model to register when the bot starts.
    Also used as a sort of "test" operation when initializing the database.
    """

    boot_at = tortoise.fields.DatetimeField(auto_now_add=True)


MAX_PREFIX_LENGTH = 10


class Bot(tortoise.Model):
    """Tortoise model describing a record of a bot in the server."""

    # Internal ID
    id: int = tortoise.fields.IntField(pk=True)
    bot_id: int = tortoise.fields.BigIntField(unique=True)
    owner_id: int = tortoise.fields.BigIntField()
    prefix: str = tortoise.fields.CharField(MAX_PREFIX_LENGTH, unique=True)
    # Message ID so we can later add a reaction when the bot is invited.
    invite_message_id: int = tortoise.fields.BigIntField(unique=True)
