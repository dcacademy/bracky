#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

__all__ = ["RoleAwareCooldownManager", "role_aware_cooldown", "staff_cooldown"]

import typing

import hikari
import lightbulb

from bracky.config import get_config


class RoleAwareCooldownManager(lightbulb.CooldownManager):
    """
    An implementation of a `CooldownManager` that takes into account the roles a user has,
    allowing them to bypass cooldowns if they certain roles.
    """

    def __init__(self, *args, roles: typing.Collection[hikari.SnowflakeishOr[hikari.Role]]):
        super().__init__(*args)

        self.roles = set(roles)

    def add_cooldown(self, context: lightbulb.Context) -> None:
        """
        This implementation of `add_cooldown` will return early if the user has any of the roles
        present in `self.roles`.
        """
        guild = hikari.Snowflake(get_config().constants.guild)
        member = context.bot.cache.get_member(guild, context.author.id)
        if member is not None and set(member.role_ids) & self.roles:
            return

        super().add_cooldown(context)


def role_aware_cooldown(*args, roles: typing.Collection[hikari.SnowflakeishOr[hikari.Role]]):
    """A little decorator that makes use of `RoleAwareCooldownManager`."""

    def decorate(command: lightbulb.Command) -> lightbulb.Command:
        command.cooldown_manager = RoleAwareCooldownManager(*args, roles)
        return command

    return decorate


def staff_cooldown(*args):
    """Convenience decorator that sets `RoleAwareCooldownManager` with the staff role."""
    return role_aware_cooldown(*args, roles={get_config().constants.staff_role})
