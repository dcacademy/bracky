#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Bunch of quick message respond functions.
"""

__all__ = ["send_success", "send_failure"]

import typing

import hikari
import lightbulb


SUCCESS_EMOJI: typing.Final[str] = "\N{SMALL BLUE DIAMOND}"
FAILURE_EMOJI: typing.Final[str] = "\N{DOUBLE EXCLAMATION MARK}"


def send_success(channel: hikari.TextChannel, message: str) -> typing.Coroutine[typing.Any, typing.Any, hikari.Message]:
    """Sends a success message to the given channel, prefixing it with a success emoji."""
    return channel.send(f">>> {SUCCESS_EMOJI} {message}")


def send_failure(channel: hikari.TextChannel, message: str) -> typing.Coroutine[typing.Any, typing.Any, hikari.Message]:
    """Sends a failure message to the given channel, prefixing it with a failure emoji."""
    return channel.send(f">>> {FAILURE_EMOJI} {message}")
