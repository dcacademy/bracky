#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

__all__ = ["ThreadedWebhookLoggerHandler"]

import logging
import threading
import typing

import requests


DEFAULT_NAME: typing.Final[str] = "Bracky Logs"
DEFAULT_MAX_WAIT_TIME: typing.Final[int] = 10
DEFAULT_MAX_PAGE_SIZE: typing.Final[int] = 1900


class ThreadedWebhookLoggerHandler(threading.Thread, logging.Handler):
    """
    Logging handler that emits records to a Discord channel via a webhook and works on a separate thread,
    so that it survives even if the main app thread runs into an exception.
    It automatically waits for enough content and paginates it in an attempt to not hit the ratelimits.
    """

    __slots__ = (
        "url",
        "name",
        "max_wait_time",
        "page_prefix",
        "page_suffix",
        "session",
        "event",
        "bits",
    )

    def __init__(
        self,
        url: str,
        *,
        name: str = DEFAULT_NAME,
        max_wait_time: typing.Union[int, float] = DEFAULT_MAX_WAIT_TIME,
        max_page_size: int = DEFAULT_MAX_PAGE_SIZE,
        page_prefix: str = "",
        page_suffix: str = "",
        log_format: typing.Optional[str] = None,
        log_date_format: typing.Optional[str] = None,
    ):
        # Initializing the thread
        threading.Thread.__init__(self, name=name, daemon=True)

        # Setting some attrs
        # The webhook URL to use. Must be a URL with token.
        self.url = url
        # The name used for the thread and when sending content to Discord.
        self.name = name
        # The maximum time (in seconds) the handler waits for new records before flushing what it has to Discord.
        self.max_wait_time = float(max_wait_time)
        #
        self.max_page_size = max_page_size
        # The prefix to add to every record page.
        self.page_prefix = page_prefix
        # The suffix to add to every record page.
        self.page_suffix = page_suffix

        self.session = requests.Session()
        self.event = threading.Event()
        # Collection of formatted logs to then glue together into pages to be flushed
        self.bits: typing.List[str] = []

        # Initializing the handler
        logging.Handler.__init__(self)
        # using the { formatting cus it's easier on the eyes
        self.setFormatter(logging.Formatter(log_format, log_date_format, "{"))

        # Starting the thread
        self.start()

    def emit(self, record: logging.LogRecord) -> None:
        """Processes logging level and and adds formatted records to the flushing queue."""
        if record.levelno >= logging.INFO:
            # Since we wait until enough records are emitted, the record date is appended just for easier tracking
            self.bits.append(self.format(record))
            self.event.set()

    def paginate(self) -> typing.List[str]:
        """Crude pagination logic, it's gonna be used until a better paginator is written."""
        # Early return in case we have empty bits.
        # This also saves us from having to try-except IndexError when clearing the bits.
        if not self.bits:
            return []

        pages: typing.List[str] = []

        # This is not very ideal, but it is good enough for while we don't get a proper paginator :/
        glued = "\n".join(self.bits)
        # We can leave the last page alone, so we don't need to add 1
        pages_amount = len(glued) // self.max_page_size
        for n in range(pages_amount):
            start = n * self.max_page_size
            pages.append(glued[start : start + self.max_page_size])

        # Don't forget to clear the used bits, keeping the last page
        self.bits = [self.bits[-1]]

        return pages

    def flush(self) -> None:
        """Flushes all records to the webhook."""
        # Reset the paginator pages before doing the requests, hence the copy
        pages_to_send = self.paginate()
        for page in pages_to_send:
            if page.strip():
                self.session.post(self.url, data={"username": self.name, "content": page})

    def run(self) -> None:
        while True:
            try:
                self.event.wait(timeout=self.max_wait_time)

                if sum(len(i) for i in self.bits) >= self.max_page_size:
                    self.flush()

            except TimeoutError:
                self.flush()
            except Exception:
                logging.exception("Encountered an exception! Closing...")

            finally:
                self.event.clear()
