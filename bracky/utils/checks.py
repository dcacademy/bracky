#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Bunch of useful and convenient checks for lightbulb.
"""

__all__ = [
    "is_contributor",
    "contributor_check",
    "is_staff",
    "staff_check",
    "is_bracky_dev",
    "bracky_dev_check",
]

from importlib import resources
import typing

import hikari
import lightbulb

from bracky import resources as bracky_resources
from bracky import config
from bracky.utils import errors

T_inv = typing.TypeVar("T_inv", bound=lightbulb.Command)

CONTRIBUTOR_IDS: typing.List[int]
STAFF_ROLE_ID: int
DEV_ROLE_ID: int


def _load_contributors() -> typing.List[int]:
    raw_contributors = resources.read_text(bracky_resources, "contributors.txt")
    contributor_ids = []
    for line in raw_contributors.split("\n"):
        if (user_id := line.strip()).isdigit():
            contributor_ids.append(int(user_id))
    return contributor_ids


def _load_role_ids() -> typing.Tuple[int, int]:
    conf = config.get_config().constants
    return conf.staff_role, conf.bracky_dev_role


CONTRIBUTOR_IDS = _load_contributors()
STAFF_ROLE_ID, DEV_ROLE_ID = _load_role_ids()


def is_contributor(user_id: int) -> bool:
    """Returns True if the user ID is present in the contributors file."""
    if user_id not in CONTRIBUTOR_IDS:
        raise errors.NotContributor()
    return True


def contributor_check() -> typing.Callable[[T_inv], T_inv]:
    """Command check based on the `is_contributor` function."""

    async def check_is_contributor(context: lightbulb.Context) -> bool:
        return is_contributor(context.author.id)

    return lightbulb.check(check_is_contributor)


def is_staff(member: hikari.Member) -> bool:
    """Returns True if the member has the Staff role."""
    if STAFF_ROLE_ID not in member.role_ids:
        raise errors.NotStaff()
    return True


def staff_check() -> typing.Callable[[T_inv], T_inv]:
    """Command check based on the `is_staff` function."""

    async def check_is_staff(context: lightbulb.Context) -> bool:
        return is_staff(context.member)

    return lightbulb.check(check_is_staff)


# We could probably mash this together with `is_contributor`
def is_bracky_dev(member: hikari.Member) -> bool:
    """Returns True if the member has the Bracky Developer role."""
    if DEV_ROLE_ID not in member.role_ids:
        raise errors.NotBrackyDev()
    return True


def bracky_dev_check() -> typing.Callable[[T_inv], T_inv]:
    """Command check based on the `is_bracky_dev` function."""

    async def check_is_bracky_dev(context: lightbulb.Context) -> bool:
        return is_bracky_dev(context.member)

    return lightbulb.check(check_is_bracky_dev)
