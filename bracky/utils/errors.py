#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

__all__ = ["BrackyCheckFailure", "NotContributor", "NotStaff", "NotBrackyDev"]

import lightbulb


class BrackyCheckFailure(lightbulb.errors.CheckFailure):
    """Base class for all bracky check failure exceptions."""

    pass


class NotContributor(BrackyCheckFailure):
    """
    Exception raised when a command marked as contributor only
    is attempted to be run by a user who is not a contributor.
    """

    pass


class NotStaff(BrackyCheckFailure):
    """
    Exception raised when a command marked as staff only
    is attempted to be run by a member who is not staff.
    """

    pass


class NotBrackyDev(BrackyCheckFailure):
    """
    Exception raised when a command marked as bracky developer only
    is attempted to be run by a member who is not a bracky developer.
    """

    pass
