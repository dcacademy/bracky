#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

__author__ = "Tmpod"
__organisation__ = "dcacademy"
__contributors__ = (__author__, "davfsa", "M.A", "thomm.o", "Forbidden")

__title__ = "Bracky"
__license__ = "GPLv3"
__version__ = "1.0.0"

__repository__ = f"https://gitlab.com/{__organisation__}/{__title__}.git"
__url__ = __repository__
